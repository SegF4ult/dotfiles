local telescope = require('telescope')
local builtin = require('telescope.builtin')

local M = {}
M.dotfiles = function()
    builtin.git_files({
        prompt_title = "< Dotfiles >",
        cwd = "$HOME/dotfiles"
    })
end

M.project_search = function()
    builtin.grep_string({search = vim.fn.input("Grep Project > ")})
end

return M
