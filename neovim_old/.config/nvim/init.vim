if has("nvim")
    let g:plug_home = stdpath('data') . '/site/plugged'
endif

call plug#begin()
    Plug 'neovim/nvim-lspconfig',

    Plug 'nvim-lua/popup.nvim',
    Plug 'nvim-lua/plenary.nvim',
    Plug 'nvim-telescope/telescope.nvim',

    Plug 'vimwiki/vimwiki',
    Plug 'tpope/vim-fugitive',

    Plug 'itchyny/lightline.vim'
    Plug 'kyazdani42/nvim-web-devicons',
    Plug 'RRethy/nvim-base16',
    Plug 'rust-lang/rust.vim'
call plug#end()

colorscheme base16-unikitty-dark
highlight Normal guibg=none
highlight NormalNC guibg=none

let g:lightline = {
    \ 'colorscheme': 'material',
    \ 'active': { 'left': [ ['mode', 'paste'], ['gitbranch', 'readonly', 'filename', 'modified'] ] },
    \ 'component_function': { 'gitbranch': 'FugitiveHead' }
\ }

execute "highlight CursorLineNr guifg=" . g:terminal_color_1

lua require('telescope').setup{}
lua require('lspconfig').pyright.setup{}
lua require('lspconfig').rust_analyzer.setup{}
