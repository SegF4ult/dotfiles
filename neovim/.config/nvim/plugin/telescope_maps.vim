let mapleader=" "
nnoremap <leader>ps <cmd>lua require('segf4ult.telescope').project_search()<cr>
nnoremap <leader>df <cmd>lua require('segf4ult.telescope').dotfiles()<cr>
