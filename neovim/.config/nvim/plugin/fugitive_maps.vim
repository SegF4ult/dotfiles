let mapleader=" "
nnoremap <leader>gs <cmd>Git<cr>
nnoremap <leader>gb <cmd>Git blame<cr>
nnoremap <leader>gd <cmd>Gvdiffsplit<cr>
