set encoding=utf8
set noerrorbells

"" Tabs/indents
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent

"" interesting: exrc

"" Line Numbering/gutter
set number
set relativenumber
set signcolumn=yes
"" Statusline/tabline/etc.
set noshowmode
set cmdheight=2
"" Usability
set completeopt=menuone,noinsert,noselect
"" rendering / search
set termguicolors
set nohlsearch
set incsearch
set colorcolumn=80
set nowrap
set scrolloff=8
"" Backups
set noswapfile
set nobackup
set undofile
"" Splits
set splitbelow
set splitright
