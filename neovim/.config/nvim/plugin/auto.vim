fun! TrimTrailingWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfun

augroup SegF4ult
    autocmd!
    autocmd BufWritePre * :call TrimTrailingWhitespace()
    autocmd! TextYankPost * silent! lua require'vim.highlight'.on_yank({higroup='Search',timeout=400})
augroup END
