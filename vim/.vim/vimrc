set encoding=utf8

set tabstop=4

if empty(glob('~/.vim/autoload/plug.vim'))
	silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
	\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Vim-plug
call plug#begin('~/.vim/plugged')

" File exploring
Plug 'preservim/nerdtree'
map <C-t> :NERDTreeToggle<CR>

" Theme
Plug 'arcticicestudio/nord-vim'
" Statusline
Plug 'itchyny/lightline.vim'
" Syntax plugins
Plug 'sirtaj/vim-openscad'
Plug 'rust-lang/rust.vim'
" Other plugins
Plug 'ryanoasis/vim-devicons'
Plug 'rust-lang-nursery/rustfmt'
Plug 'majutsushi/tagbar'
" Git Plugins
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
"" <previm>
Plug 'kannokanno/previm'
let g:previm_open_cmd = 'firefox'
"" </previm>

"" vim-latex-live-preview
Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }
let g:livepreview_cursorhold_recompile = 0
let g:livepreview_previewer = 'zathura'

call plug#end()

"" Statusline
set laststatus=2

colorscheme torte

let g:lightline = {
	\ 'colorscheme': 'wombat',
	\ 'active': {
	\ 	'left': [ [ 'mode', 'paste' ], [ 'fugitive','filename'] ],
	\ },
	\ 'component_function': {
	\	'fugitive': 'LightlineFugitive',
	\	'filename': 'LightlineFilename',
	\	'filetype': 'LightlineFiletype',
	\	'fileformat': 'LightlineFileformat',
	\ },
	\ 'separator': {'left': '', 'right': '' },
	\ 'subseparator': { 'left': '', 'right': '' }
	\ }

function! LightlineModified()
  if &filetype == "help"
    return ""
  elseif &modified
    return "+"
  elseif &modifiable
    return ""
  else
    return ""
  endif
endfunction

function! LightlineReadonly()
  if &filetype == "help"
    return ""
  elseif &readonly
    return "RO"
  else
    return "RW"
  endif
endfunction

function! LightlineFugitive()
	if exists("*fugitive#head")
		let branch = fugitive#head()
		return branch !=# '' ? ' '.branch : ''
	endif
	return ''
endfunction

function! LightlineFilename()
  return ('' != LightlineReadonly() ? LightlineReadonly() . ' ' : '') .
    \    ('' != expand('%:t') ? expand('%:t') : '[No Name]') .
    \    ('' != LightlineModified() ? ' ' . LightlineModified() : '')
endfunction

function! LightlineFiletype()
	return winwidth(0) > 70 ? (strlen(&filetype) ? &filetype . ' ' . WebDevIconsGetFileTypeSymbol() : 'no ft') : ''
endfunction

function! LightlineFileformat()
	return winwidth(0) > 70 ? (&fileformat . ' ' .	WebDevIconsGetFileFormatSymbol()) : ''
endfunction
"" /Statusline

"" <TagBar>
nmap <F8> :TagbarToggle<CR>
"" </TagBar>

"" Syntax and linenumbers
syntax on
set number
set relativenumber

"" Highlight and incremental search
set hlsearch
set incsearch

set showmode
set showcmd

set ttyfast
set lazyredraw

set splitbelow
set splitright

set cursorline
set wrapscan

hi Comment ctermfg=1
hi Visual cterm=NONE ctermbg=8 ctermfg=NONE
hi Folded ctermbg=NONE

"" Characters to visualize different types of whitespace
set listchars=eol:¬,tab:>-,extends:>,precedes:< list


"" Keybinds
autocmd FileType python nnoremap <buffer> <F9> :exec '!python' shellescape(@%, 1)<cr>
